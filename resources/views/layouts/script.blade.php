<script src="{{ asset('/lib/jquery-3/jQuery-v3-1-1.min.js') }}"></script>
<script src="{{ asset('/lib/bootstrap-3/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/lib/bootstrap-3/js/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript">
    $(function () {
        $('#fecha').datetimepicker();
    });
</script>
@yield('script')
