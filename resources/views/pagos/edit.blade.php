<?php
/**
 * Calculo del día anterior
 */
$fecha = date('Y-m-d');
function fecha($fecha){
    $dia = date('d');
    $mes = date('m');
    $ano = date('Y');
    $fecha = $ano . "-" .$mes ."-". $dia;
    return $fecha;
}
?>
@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <!-- begin #content -->
            <div id="content" class="content">

                <div class="col-md-12">
                    <div class="panel panel-default">
                        @include('users.delete')
                        <div class="panel-heading">
                            Usuarios Pagos
                        </div>
                        <div class="panel-body">

                        @include('layouts.errors')
                        @include('layouts.success')

                        <!-- begin #content -->
                            <div id="content" class="content">
                                <!-- begin breadcrumb -->
                                <ol class="breadcrumb pull-right">
                                    <li><a href="{{url('/home')}}">Home</a></li>
                                    <li class="active">Usuarios Pagos</li>
                                </ol>
                                <!-- end breadcrumb -->
                                <!-- begin page-header -->
                                <h1 class="page-header">Mantenedor <small>Usuarios Pagos</small></h1>
                                <!-- end page-header -->
                                <form action="{{url('/pagos/'. Hashids::encode($user->id) .'/update' )}}" enctype="multipart/form-data" method="post">
                                    <!-- Select -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>
                                                Usuarios
                                            </label>
                                            <input type="text" class="form-control" value="{{ $user->name }}" readonly="readonly" name="usuario">
                                        </div>
                                    </div>

                            <!-- Select -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>
                                        Pagos
                                    </label>
                                    <select  class="form-control" id="users" name="pagos" >
                                        @if(isset($codPago))
                                            @foreach($codPago as $data)
                                                @if(empty($data))
                                                    <option value="">vacio</option>
                                                    @else
                                                        <option value="{{ $data->cod_pago }}" selected="selected">cod: {{ $data->cod_pago }} -- ${{ $data->importe }}</option>
                                                    @endif
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <!-- Importe -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>
                                        Importe
                                    </label>
                                    <input  class="form-control"  name="importe" id="importe" type="number"  min="1" max="999999"  placeholder="$" required>
                                </div>
                            </div>
                            <!-- Fecha -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>
                                        Fecha
                                    </label>
                                    <input class="form-control" type="date" name="fecha" id="fecha" min="<?php echo fecha($fecha); ?>" placeholder="AAAA-DD-MM"required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                <input name="_action" type="hidden" value="editar">
                                <br>
                                <button class=" btn  btn-primary" id="enviar" type="submit">
                                    Guardar
                                </button>
                                </input>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection