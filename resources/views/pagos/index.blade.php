@extends('layouts.master')
@section('content')

<div class="container">
    <div class="row">
        <!-- begin #content -->
        <div id="content" class="content">

            <div class="col-md-12">
                <div class="panel panel-default">
                    @include('users.delete')
                    <div class="panel-heading">
                        Usuarios Pagos
                    </div>
                    <div class="panel-body">

                    @include('layouts.errors')
                    @include('layouts.success')

                    <!-- begin #content -->
                    <div id="content" class="content">
                        <!-- begin breadcrumb -->
                        <ol class="breadcrumb pull-right">
                            <li><a href="{{url('/home')}}">Home</a></li>
                            <li class="active">Usuarios Pagos</li>
                        </ol>
                        <!-- end breadcrumb -->
                        <!-- begin page-header -->
                        <h1 class="page-header">Mantenedor <small>Usuarios Pagos</small></h1>
                        <!-- end page-header -->
                        <div style="padding-bottom: 15px;text-align: right">
                            <a href="{{url('pagos/create')}}" class="btn btn-primary" id="crear-usuario" title="Crear Pago"><i class="glyphicon glyphicon-plus"></i></a>
                        </div>
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <th>#</th>
                                <th >Nombre</th>
                                <th>Valor</th>
                                <th>Acciones</th>
                            </thead>
                            @foreach($users as $user)
                                @if(!$user->hasRole( 'admin') )
                                    <tbody>
                                        <td>{{ $loop->index  + 1}}</td>
                                        <td>
                                            {{ $user->name }}

                                        </td>
                                        <td>
                                            <div class="input-group mb-3">
                                               <?php
                                                $sumPagos = new \App\Pagos();
                                                $sumPagos = $sumPagos->sumPago($user->id);
                                                ?>
                                                @foreach($sumPagos as $data)
                                                    @if(empty($data->suma))
                                                           No tiene Deuda
                                                    @else
                                                        ${{ $data->suma }}
                                                    @endif
                                                @endforeach
                                            </div>
                                        </td>
                                    <td>
                                        <!-- Editar pago-->
                                        <a id="editar-roles" class="btn btn-xs btn-primary" href="{{ url('pagos/'.Hashids::encode($user->id).'/edit') }}" title="Pagar"><span class="glyphicon glyphicon-usd" aria-hidden="true"></span></a>
                                        <!-- Alta -->
                                        <a id="eliminar-usuario" class="btn btn-xs btn-success" href="{{url('pagos/show')}}"  title="Alta pago"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></a>
                                    </td>

                                </tr>
                                </tbody>
                                @endif
                        @endforeach
                        </table>
                    </div>
                        <div style="text-align: center">
                            {{ $users->appends($_GET)->render() }}
                        </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection