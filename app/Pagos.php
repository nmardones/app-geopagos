<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class Pagos extends Model
{
    //

    /**
     * @param $id
     * @return mixed
     */
    public function codPago($id){
        $sql = "select pag.cod_pago
                      , pag.importe
                  from pago pag
                  join usuarios_pagos usup
                    on pag.cod_pago = usup.cod_pago
                  join users usu
                  on usu.id = usup.cod_usuario
                  where usu.id =".$id;
        $codPago = DB::select($sql);
        return $codPago;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function sumPago($id){
        $sql = "select  SUM(pag.importe) suma
                  from pago pag
                  join usuarios_pagos usup
                    on pag.cod_pago = usup.cod_pago
                  join users usu
                  on usu.id = usup.cod_usuario
                  where usu.id =" .$id;
        $sumPago = DB::select($sql);
        return $sumPago;
    }
}
