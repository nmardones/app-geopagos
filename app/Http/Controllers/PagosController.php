<?php

namespace App\Http\Controllers;

use App\Http\Requests\PagoRequest;
use Illuminate\Http\Request;
use App\User;
use App\Pagos;
use Hashids;
use DB;

class PagosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nombre = $request->get("nombre");
        $users = User::nombre($nombre)->orderBy('id', 'ASC')->paginate(5);
       return view('pagos.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return view('pagos.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PagoRequest $request)
    {
        try{
            $importe = $request->get('importe');
            $fecha = $request->get('fecha');
            $sql = "insert into pago(importe,fecha) values('$importe','$fecha')";
            DB::select($sql);
            $usersId = $request->get('users');
            $sql = "SELECT MAX(cod_pago) cod_id FROM pago";
            $lastId = DB::select($sql);
            $lastId = $lastId[0]->cod_id;
            foreach ($usersId as $data){
                $sql = "insert into usuarios_pagos values('$lastId','$data')";
                DB::select($sql);
            }
            return redirect('pagos')->with('success','Se ha pagado correctamente');
        } catch (Exception $ex) {
            return redirect('pagos')->with('errors', $ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('pagos')->with('success','ha sido dado de alta correctamente');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $decode = Hashids::decode($id)[0];
        $user = User::find($decode);
        $codPago = new Pagos;
        $codPago = $codPago->codPago($decode);
        return view('pagos.edit',compact('user','codPago'));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $importe = $request->get('importe');
            $fecha = $request->get('fecha');
            $codPago = $request->get('pagos');
            $sql = "update pago set importe = importe -". $importe . " where cod_pago =". $codPago;
            DB::select($sql);
            return redirect('pagos')->with('success','Se ha pagado correctamente');
        } catch (Exception $ex) {
            return redirect('pagos')->with('errors', $ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
