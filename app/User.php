<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Auth;
use DB;


class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles(){
        return $this->belongsToMany('App\Role','role_user','user_id','role_id');
    }

    /**
     *    Para filtrar por nombre
     */
    public function scopeNombre($query, $name)
    {
        if (trim($name) != '') {
            $query->where('name', 'LIKE' , "%$name%");
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getRolById($id)
    {
        $sql = "SELECT role_id FROM role_user WHERE user_id=". $id;
        $rolUser = DB::select($sql);
        return $rolUser;
    }

    /***
     * @param $id
     * @return mixed
     */
    public function getFavoritosById($id)
    {
        $sql = "SELECT user_fav_id FROM favoritos WHERE user_admin_id=". $id;
        $rolUser = DB::select($sql);
        return $rolUser;
    }

    /**
     * @param $id
     * @return mixed
     *
     */
    public function showFavoritos($id){
        $sql = "select  fav.user_fav_id
                    from users usu
                    join favoritos fav
                    on usu.id = fav.user_admin_id
                    where usu.id =". $id;
        $showFavoritos = DB::select($sql);
        return $showFavoritos;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function favoritosName($id){
        $sql = "SELECT name from users where id=" .$id;
        $favoritoName = DB::select($sql);
        return $favoritoName;
    }

}
