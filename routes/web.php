<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/* Users */
Route::resource('users', 'UserController');
Route::get('users/destroy/{id}', 'UserController@destroy');
Route::post('users/{id}/update', 'UserController@update');
Route::get('users/{id}', 'UserController@index');

/* Roles */
Route::resource('roles', 'RoleController');
Route::get('roles/destroy/{id}', 'RoleController@destroy');
Route::post('roles/{id}/update', 'RoleController@update');
Route::post('roles/store', 'RoleController@store');

/*Pagos*/
Route::resource('pagos','PagosController');
Route::get('pagos/destroy/{id}', 'PagosController@destroy');
Route::post('pagos/{id}/update', 'PagosController@update');
Route::post('pagos/store', 'PagosController@store');
Route::post('pagos/show', 'PagosController@show');
